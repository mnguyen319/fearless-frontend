function createCard(title, description, pictureUrl, formattedStarts, formattedEnds, location) {
    return `<div class="card">
    <img src="${pictureUrl}" class="card-img-top" alt="...">
    <div class="card-body">
      <h5 class="card-title">${title}</h5>
      <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
      <p class="card-text">${description}</p>
    </div>
    <div class="card-footer">
    ${formattedStarts} - ${formattedEnds}
    </div>
  </div>`;
}

function createAlert(message) {
    return `
    <div class="alert alert-danger" role="alert">
    ${message}
    </div>`;
}

function createPlaceholder() {
    return `
      <div
      class="card shadow mb-5 bg-white rounded"
      style="height: min-content"
    >
      <div
        class="card-img-top placeholder-glow"
        style="width: 100%; height: 200px"
      >
        <div
          class="placeholder"
          style="height: 100%; width: 100%"
        ></div>
      </div>
      <div class="card-body">
        <h5 class="card-title placeholder-glow">
          <span class="placeholder col-6"></span>
        </h5>
        <h6 class="card-subtitle mb-2 text-muted placeholder-glow">
          <span class="placeholder col-6"></span>
        </h6>
        <p class="card-text placeholder-glow">
          <span class="placeholder col-7"></span>
          <span class="placeholder col-4"></span>
          <span class="placeholder col-4"></span>
          <span class="placeholder col-6"></span>
          <span class="placeholder col-8"></span>
        </p>
      </div>
      <div class="card-footer placeholder-glow">
        <span class="placeholder col-7"></span>
      </div>
    </div>
      `;
  }
  
  


window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    
    try {
        function updateColumnDOM(column, elementsToRender){
            column.innerHTML = elementsToRender.join("");
        }

        const column = document.querySelector(".col");

        const elementsToRender = [createPlaceholder()];
        
        const response = await fetch(url);

        if (!response.ok) {
            // return Promise.reject(response)
            throw new Error("response is bad")
        } else {
            elementsToRender.pop();

            const data = await response.json();

            for (let conference of data.conferences) {
                elementsToRender.push(createPlaceholder());

                updateColumnDOM(column, elementsToRender);

                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok){
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const starts = details.conference.starts;
                    const dateStarts = new Date(starts);
                    const formattedStarts = `${
                        dateStarts.getMonth() + 1
                    }/${dateStarts.getDate()}/${dateStarts.getFullYear()}`;
                    const ends = details.conference.ends;
                    const dateEnds = new Date(ends);
                    const formattedEnds = `${
                        dateEnds.getMonth() + 1
                    }/${dateEnds.getDate()}/${dateEnds.getFullYear()}`;
                    const location = details.conference.location.name;
                    const html = createCard(title, description, pictureUrl, formattedStarts, formattedEnds, location);
                    elementsToRender.pop();
                    elementsToRender.push(html);
                    updateColumnDOM(column, elementsToRender);
                    

                }
            }
        }
    } catch (e) {
        console.error('error', e);
        const alertHtml = createAlert(e.message);
        const row = document.querySelector(".row");
        row.innerHTML = alertHtml + row.innerHTML;

    }
});
    // const response = await fetch(url);

    // if (!response.ok) {
    //     return Promise.reject()
    // } else {
    //     const data = await response.json()
    // }
 


